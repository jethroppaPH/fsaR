# fsaR :fish:

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![pipeline status](https://gitlab.com/jethroppaPH/fsaR/badges/main/pipeline.svg)](https://gitlab.com/jethroppaPH/fsaR/-/commits/main)

## Disclaimer

This package is an independent initiative and is not affiliated with or sponsored by any institutions.

## Overview

This package offers a collection of easy-to-use R tools designed to simplify the preparation, processing, and visualization of fish stock assessment data. These helpful utilities make data management tasks easier, providing a smooth and efficient experience for all users.

## Installation

``` r
# install.packages("remotes")
remotes::install_gitlab("jethroppaPH/fsaR")
```

`data.table` is a core component of `fsaR` and may be installed using:

```r
install.packages("data.table")
```

You can learn more about `data.table` [here](https://github.com/Rdatatable/data.table#getting-started). 

### Functions

|   Function   |   Description   |
|--------------|-----------------|
|  `catchRaising()`  | Simplifies the computation of monthly and annual raising factors, allowing for the straightforward determination of both monthly and annual raised catch based on these results. |
| `estLm()` | Estimate length-at-first maturity from reproductive biology data. See this [Wiki](https://gitlab.com/jethroppaPH/fsaR/-/wikis/Estimate-Length-at-First-Maturity) for a short tutorial. |
| `estNatM()` | Estimates natural mortality using different published methods, with method classification (recommended, theoretical, non-recommended). See Maunder et al. (2023) for more details, and this [Wiki](https://gitlab.com/jethroppaPH/fsaR/-/wikis/Estimate-Natural-Mortality) for a short tutorial. |
|  `estParam()`  | Estimates several length-based parameters, including $L_{c_{\text{opt}}}$, $L_{FM}$, $L_{mean}$, $L_{opt}$, $L_{m}$, $\varphi$', $t_{0}$, and $t_{max}$. See this [Wiki](https://gitlab.com/jethroppaPH/fsaR/-/wikis/Estimate-Several-Length-Based-Parameters) for a short tutorial. |
|  `FSLBA()`  | Specifically designed for generating plots that depict the frequency distribution of lengths, including the proportions corresponding to mature, mega-spawner, and optimum lengths according to Froese (2004). In addition, it calculates the average size (Lbar) of species of interest per year. See this [Wiki](https://gitlab.com/jethroppaPH/fsaR/-/wikis/Estimate-Sustainability-Indicators-and-Average-Size-(Lbar)-from-Length-Frequency-Data) for a short tutorial. |
| `initLinf()` | Uses the empirical formulas of Pauly (1983) and Froese and Binohlan (2000) to calculate the estimated value for asymptotic length. |
|  `lengthRaising()`  | Streamlines the process of merging length frequency data with catch data, facilitating the accurate computation of both the total weight of the species (TWSp) and the total weight of the sample (TWS). It has an option to indicate if boat name is required or not. |
| `lengthToAge()` | Estimate the age based on the VBGF for any given length. The equation used is extracted from FishBase's empirical equations spreadsheet, which is derived from the works of Froese and Binohlan (2000). |
| `ssdlPrep` | Perform automatic preparation of data as input to the [SS-DL tool](https://github.com/shcaba/SS-DL-tool). See this [Wiki](https://gitlab.com/jethroppaPH/fsaR/-/wikis/Data-Preparation-for-SS-DL-Tool) for a short tutorial on how to use this function. |

Kindly review the documentation provided with each package attentively (ex. `?catchRaising`).

### References
Beverton RJH. 1992. Patterns of reproductive strategy parameters in some marine teleost fishes. Journal of Fish Biology. 41(sb):137–160. doi:10.1111/j.1095-8649.1992.tb03875.x.

Froese R. 2004. Keep it simple: three indicators to deal with overfishing. Fish and Fisheries. 5(1):86–91. doi:10.1111/j.1467-2979.2004.00144.x.

Froese R, Binohlan C. 2000. Empirical relationships to estimate asymptotic length, length at first maturity and length at maximum yield per recruit in fishes, with a simple method to evaluate length frequency data. Journal of Fish Biology. 56(4):758–773. doi:10.1111/j.1095-8649.2000.tb00870.x.

Froese R, Winker H, Gascuel D, Sumaila UR, Pauly D. 2016. Minimizing the impact of fishing. Fish Fish. 17(3):785–802. doi:10.1111/faf.12146.

Maunder MN, Hamel OS, Lee H-H, Piner KR, Cope JM, Punt AE, Ianelli JN, Castillo-Jordán C, Kapur MS, Methot RD. 2023. A review of estimation methods for natural mortality and their performance in the context of fishery stock assessment. Fisheries Research. 257:106489. doi:10.1016/j.fishres.2022.106489.

Pauly D. 1983. Some simple methods for the assessment of tropical fish stocks. Rome: Food and Agriculture Organization of the United Nations (FAO fisheries technical paper). https://www.fao.org/3/X6845E/X6845E00.htm.

Pauly D. 1984. Fish population dynamics in tropical waters: a manual for use with programmable calculators. Manila, Philippines: ICLARM (ICLARM Studies and Reviews).

Pauly D, Munro JL. 1984. Once more on the comparison of growth in fish and invertebrates. Fishbyte. 2(1):1–21.

## Wiki Pages

Short tutorials on using the functions can be found here: [Wiki](https://gitlab.com/jethroppaPH/fsaR/-/wikis/pages).

## Questions / Issues

In case you have questions or find bugs, and wanted to contribute, please write an email to [Jethro Emmanuel](mailto:jethroemmanuel@gmail.com) or post on [fsaR/issues](https://gitlab.com/jethroppaPH/fsaR/-/issues). Your valuable suggestions to enhance this package are eagerly welcomed and appreciated.

