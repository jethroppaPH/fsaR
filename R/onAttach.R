.onAttach = function(libname, pkgname){
  
  packageStartupMessage("fsaR version ", utils::packageVersion(pkgname), ". ", appendLF = FALSE)
  packageStartupMessage("Visit the package homepage at https://gitlab.com/jethroppaPH/fsaR")
}