#' @title Estimate Length-at-First Maturity
#' 
#' @description
#' This function estimates length-at-first maturity from reproductive biology
#' data.
#' 
#' @param x A CSV file containing the sex, maturity stages, and length data.
#' @param species The name of the species in scientific name. Make sure the
#' spelling is correct.
#' @param ci The class interval to be used for length data. The default value is 0.5.
#' @param family The font family to use. Default is "sans."
#' @param ... Further graphical parameters may also be supplied as arguments. See Details.
#'  
#' @details
#' The data needed must at least contain the following columns: \emph{sp_name}, 
#' \emph{sex}, \emph{maturity}, and \emph{length}. The column names must match 
#' those in your file, so keep that in mind. Likewise, the maturity stages must 
#' be represented by Roman numerals (I, II, III, IV, V). Lastly, the values in 
#' the sex column should be formatted as "F" for female and "M" for male.
#' 
#' Graphical parameters commonly used are `lwd`, `lty`, `pch`, `cex`, and `col`. 
#' For more information, see `?par` and `?points`.
#' 
#' @return A value for Lm50 and Lm95.
#' 
#' @import data.table
#' 
#' @export
#' @examples
#' \dontrun{
#'
#' library(data.table)
#' library(fsaR)
#' 
#' estLm(x, species = "Decapterus macrosoma")
#'
#' }
#'
estLm <- function(x, species, ci = 0.5, family = "sans", ...) {
  
  # Filter data based on species and sex
  x <- x[sp_name == species & sex == "F"]
  
  # Compute midlength
  .binData(x, colname = "length", ci = ci)
  
  # Select only the rows with stage I to V
  x <- x[maturity %in% c("I", "II", "III", "IV", "V")]
  
  # Pivot data
  x_pivot <- dcast(x, lower + upper + midlength ~ maturity, fun.aggregate = length)
  
  # Compute sum of all mature stages (II to V) and total number of samples
  x_pivot[, sum_mat := rowSums(as.matrix(.SD)), .SDcols = c("II", "III", "IV", "V")]
  x_pivot[, total := rowSums(as.matrix(.SD)), .SDcols = c("I", "II", "III", "IV", "V")]
  x_pivot[, SLobs := round(sum_mat / total, digits = 3)]
  
  # Select required columns for Lm computation
  x_Lm <- x_pivot[, list(lower, SLobs)]
  x_Lm[, y := round(log(1 / SLobs - 1), digits = 4)]
  setnames(x_Lm, old = c("lower"), new = c("x"))
  
  # Extract the SLobs and save as a vector
  vec <- x_Lm[, SLobs]
  
  # Find the longest increasing sequence
  result <- .longestIncreasingSeq(vec)
  
  # Extract the longest consecutive sequence
  regLm <- x_Lm[result[[2]]]
  regLm <- regLm[is.finite(rowSums(regLm)), ]
  
  # Perform linear regression
  model <- lm(regLm$y ~ regLm$x)
  model_coef <- coefficients(model)
  intercept <- as.numeric(model_coef[1])
  slope <- as.numeric(model_coef[2])
  
  # Compute for SLest
  x_Lm[, SLest := round(1 / (1 + (exp(intercept + slope * x))), digits = 3)]
  
  # Find the second-to-last and last values in the 'x' column
  second_to_last <- regLm[nrow(regLm) - 1, x]
  last_value <- regLm[nrow(regLm), x]
  
  # Compute Lm50 and Lm95
  Lm50 <- as.numeric(round(intercept / abs(slope), digits = 2))
  Lm95 <- round(mean(second_to_last, last_value), digits = 2)
  
  # Separate plot and points parameters:
  # 'dots' collects all additional arguments passed via '...'
  # 'points_params' filters out arguments specifically for points (e.g., color, point type, size)
  # 'plot_params' contains the remaining arguments for the plot (excluding those for points)
  dots <- list(...)
  points_params <- dots[names(dots) %in% c("col", "pch", "cex")]
  plot_params <- dots[!names(dots) %in% c("col", "pch", "cex")]
  
  # Extract lty and pch for the legend
  line_type <- ifelse(is.null(plot_params$lty), 1, plot_params$lty)
  point_char <- ifelse(is.null(points_params$pch), 1, points_params$pch)
  
  # PLOT
  par(omi = c(0.5, 0.2, 0.75, 0.2), mai = c(0.5, 0.75, 0.25, 0.25), las = 1,
      family = family)
  do.call(plot, c(list(x = x_Lm$x, y = x_Lm$SLest, type = "l", xlab = "", ylab = ""), plot_params))
  
  # Add points with separate parameters
  do.call(points, c(list(x = regLm$x, y = regLm$SLobs), points_params))
  
  mtext("Length-at-First Maturity Estimate", side = 3, line = 2.3, adj = 0, 
        cex = 1.2, font = 2, outer = TRUE)
  mtext(species, side = 3, line = 1, adj = 0, cex = 1.2, font = 3, outer = TRUE)
  mtext("Fraction Retained", side = 2, adj = 0.5, las = 0, outer = TRUE, line = -1)
  mtext("Length (cm)", side = 1, adj = 0.5, outer = TRUE, xpd = TRUE, line = 0.25)
  
  # Add Legend reflecting the user's input for pch and lty
  legend("bottomright", inset = 0.05, legend = c("SL est", "SL obs"), 
         lty = c(line_type, NA), pch = c(NA, point_char), lwd = 2, bty = "n", 
         cex = 0.8, text.font = 3)
  
  # Add text labels
  text(min(x_Lm$x), 0.98, paste("Lm50 = ", Lm50, "cm"), cex = 0.8, pos = 4)
  text(min(x_Lm$x), 0.94, paste("Lm95 = ", Lm95, "cm"), cex = 0.8, pos = 4)
}
