#' @title Fish Stock Length-Based Analysis
#' 
#' @description
#' This function generates sustainability indicators using length-frequency 
#' data and length at maturity, based on Froese (2004). It also calculates the 
#' average size (Lbar) of species of interest per year.
#'
#' @param x The data containing the lengths. See \strong{Examples} for more
#' information
#' @param species The scientific name of the species. Make sure the spelling
#' is correct. (Character)
#' @param Linf Asymptotic length of the species. From VBGF.
#' @param K Growth coefficient of the the species. From VBGF.
#' @param M Natural mortality.
#' @param Lm Length-at-first maturity.
#' @param ci Class interval to use.
#' @param family The font family to use. Default is "sans."
#' @param ... Further graphical parameters may also be supplied as arguments. See Details.
#'
#' @details 
#' The data needed must at least contain the following columns: \emph{year},
#' \emph{sp_name}, \emph{length}, \emph{lf}, \emph{tws}, and \emph{twsp}. The 
#' column names must match those in your file, so keep that in mind. 
#' 
#' \itemize{ \item year - The year when data was collected \item sp_name - The 
#' name of the species \item length - The actual length of the species extracted 
#' from the NSAP database \item lf - The length frequency extracted from the 
#' NSAP database \item tws and twsp - Needed to raise the length data}
#' 
#' Graphical parameters commonly used are `lwd`, `pch`, `cex`, and `col`. 
#' For more information, see `?par` and `?points`.
#' 
#' @return A plot of length frequencies with the percentages of mature,
#' mega-spawners, and the optimum lengths. It also produces a table with the 
#' values for Lc and Lbar, as well as plots for the changes in Lc, Lbar, F, M, 
#' and Z over time.
#' 
#' @references 
#' Froese R. 2004. Keep it simple: three indicators to deal with overfishing. 
#' Fish and Fisheries. 5(1):86–91. doi:10.1111/j.1467-2979.2004.00144.x.
#'
#' @import data.table graphics stats grDevices utils
#'
#' @export
#' @examples
#' \dontrun{
#' 
#' library(fsaR)
#' library(data.table)
#' 
#' x <- fread("length_data.csv")
#'
#' names(x)
#' # "year" "sp_name" "length" "lf" "twsp" "tws"
#'
#' FSLBA(x = x, species = "Decapterus macarellus", Linf = 42.64, K = 1.73,
#' M = 2.3, Lm = 19.92, ci = 1)
#'
#' }
#'

FSLBA <- function(x, species, Linf, K, M, Lm, ci, family = "sans", ...) {
  
  # USER INPUT & SETUP
  
  # Set up output directory
  dir_output <- "output/"
  if (!dir.exists(dir_output)) {
    dir.create(dir_output)
  }
  
  # DATA PREPARATION
  
  # Ensure data is a data.table for efficient operations
  if (!is.data.table(x)) {
    x <- setDT(x)
  }
  
  # Select the needed columns
  x <- x[, .(year, sp_name, length, lf, twsp, tws)]
  
  # Filter and prepare species-specific data
  # Ensure to use the correct column name for filtering
  x <- x[sp_name == species]  
  
  # Remove the row(s) with length equal to zero
  x[, length := fifelse(is.na(length), 0, length)]
  
  # Compute raised LF
  x[, raised_lf := round((twsp / tws) * lf, digits = 2)]
  
  # Bin the data
  .binData(x, "length", ci = ci)
  
  # Pivot data for analysis
  xpivot <- dcast(
    data = x, formula = midlength ~ year,
    fun.aggregate = sum, value.var = "raised_lf"
  )
  
  xpivot <- melt(
    data = xpivot, id.vars = "midlength",
    value.name = "value", variable.name = "year"
  )
  
  # Remove rows with missing values
  xpivot <- na.omit(xpivot)  
  
  # Calculate Lopt and related length thresholds (shared by both analyses)
  Lopt <- round(Linf * (3 / (3 + (M / K))), digits = 2)
  LoptMinus10 <- round(Lopt * 0.9, digits = 2)
  LoptPlus10 <- round(Lopt * 1.1, digits = 2)
  
  # FROESE LBI ANALYSIS
  
  # Filter lengths into categories: Juvenile, Mature, Lopt, Mega-Spawners
  lengthJuv <- xpivot[midlength < Lm]
  lengthMat <- xpivot[midlength >= Lm]
  lengthLopt <- xpivot[midlength >= LoptMinus10 & midlength <= LoptPlus10]
  lengthMega <- xpivot[midlength > LoptPlus10]
  
  # Calculate sums and percentages per year for each category
  sumAll <- xpivot[, list(sumAll = sum(value)), by = c("year")]
  sumJuv <- lengthJuv[, list(sumJuv = sum(value)), by = c("year")]
  sumMat <- lengthMat[, list(sumMat = sum(value)), by = c("year")]
  sumLopt <- lengthLopt[, list(sumLopt = sum(value)), by = c("year")]
  sumMega <- lengthMega[, list(sumMega = sum(value)), by = c("year")]
  
  froese_dat <- Reduce(
    function(...) {
      merge(..., all.x = TRUE)
    },
    list(sumAll, sumJuv, sumMat, sumLopt, sumMega)
  )
  
  froese_dat[, prcntJuv := round((sumJuv / sumAll) * 100, digits = 2)]
  froese_dat[, prcntMat := round((sumMat / sumAll) * 100, digits = 2)]
  froese_dat[, prcntLopt := round((sumLopt / sumAll) * 100, digits = 2)]
  froese_dat[, prcntMega := round((sumMega / sumAll) * 100, digits = 2)]
  
  # AVERAGE LENGTH (LBAR)
  
  # Determine the mode per year
  # The mode per year will be the value for Lc
  LcYr <- setorder(xpivot, -value)[, head(.SD, 1), keyby = year]
  LcYr[, value := NULL]
  colnames(LcYr)[2] <- "Lc"
  
  # Merge the modes back to the original data
  xpivot <- merge(xpivot, LcYr, by = "year")
  xpivot <- xpivot[order(year, midlength)]
  
  # Filter the data to include midlength from mode to Linf
  lbar_dat <- xpivot[midlength >= Lc & midlength <= Linf]
  
  # Remove the Lc column as it's no longer needed in this data
  lbar_dat[, Lc := NULL]
  
  # Compute the total value per year
  lbar_dat[, total_value := sum(value), by = year]
  
  # Compute the proportion of value per year for each row
  lbar_dat[, length_props := round(value / total_value, digits = 5)]
  
  # Compute the weighted average for each year (Lbar)
  LbarYr <- lbar_dat[
    , 
    .(Lbar = round(weighted.mean(midlength, length_props), digits = 2)), 
    by = year
  ]
  
  # Merge the data frames
  result <- merge(LcYr, LbarYr, by = "year")
  
  # Compute for Z
  result[, Z := round(K * (Linf - Lbar) / (Lbar - Lc), digits = 2)]
  
  # Compute for F
  result[, Fm := round(Z - M, digits = 2)]
  
  # Compute for F/M
  result[, "F/M" := round(Fm / M, digits = 2)]
  
  # Compute for E
  result[, "E" := round(Fm / Z, digits = 2)]
  
  # OUTPUT
  
  # Print combined results to console
  message("User inputs: ")
  cat("Species: ", species, "\n")
  cat("Linf =", Linf, "cm ; K =", K, "/year ; M =", M, "/year ; Lm =", Lm, "cm\n")
  cat("-----------------------------------\n")
  message("\nLopt and Lopt Range:")
  cat("Lopt =", Lopt, "cm (", LoptMinus10, "-", LoptPlus10, "cm)\n")
  message("\nRESULTS (FROESE LBI):")
  print(as.data.frame(froese_dat))
  message("\nRESULTS (LBAR):")
  print(as.data.frame(result))
  
  # Write combined results to CSV files
  fwrite(froese_dat, paste0(dir_output, gsub(" ", "-", tolower(species)), "-froese-LBI.csv"))
  fwrite(result, paste0(dir_output, gsub(" ", "-", tolower(species)), "-LBAR.csv"))
  
  # PLOTTING
  
  maxL <- max(x$length)
  
  minYear <- min(x$year)
  maxYear <- max(x$year)
  
  # Determine the range of years and extend the maximum year slightly
  datayears <- as.numeric(froese_dat$year)
  extendedyears <- range(datayears)
  
  # Extend the maximum year by 1
  extendedyears[2] <- extendedyears[2] + 1
  
  # Adjust margins for legend and title
  par(
    omi = c(0.75, 0.3, 0.5, 0.2), 
    mai = c(0.25, 0.75, 0.5, 0.25), 
    las = 1, 
    family = family
  )
  
  # FROESE LBI Plots
  
  # Plot the percent juvenile and mature per year
  plot(
    x = as.numeric(froese_dat$year), 
    y = froese_dat$prcntJuv,
    type = "n", 
    axes = FALSE,
    ylim = c(0, 100), 
    xlim = extendedyears,
    xlab = "", 
    ylab = "",
    ...
  )
  
  # Lines and Points
  lines(
    x = froese_dat$year,
    y = froese_dat$prcntJuv,
    type = "o",
    lty = 2,
    ...
  )
  
  lines(
    x = froese_dat$year, 
    y = froese_dat$prcntMat, 
    type = "o", 
    lty = 1,
    ...
  ) 
  
  # Axes
  axis(
    side = 1, 
    at = datayears, 
    labels = froese_dat$year, 
    line = 0.8, 
    tcl = -0.3
  )
  
  axis(
    side = 2, 
    at = seq(0, 100, 10), 
    labels = seq(0, 100, 10), 
    las = 1, 
    line = 0.5, 
    tcl = -0.3
  )
  
  # Add plot title
  mtext(
    text = "Juvenile and Mature Fish", 
    side = 3, 
    line = 0.5, 
    adj = 0, 
    cex = 1.2, 
    outer = TRUE, 
    font = 2
  )
  
  # Add plot subtitle
  mtext(
    text = species, 
    side = 3, 
    line = -0.65, 
    adj = 0, 
    font = 3, 
    cex = 1.1, 
    outer = TRUE
  )
  
  # Add y-axis title
  mtext(
    text = "Percent in Catch", 
    side = 2, 
    adj = 0.5, 
    las = 0, 
    outer = TRUE, 
    line = -0.1
  )
  
  # Add x-axis title
  mtext(
    text = "Years", 
    side = 1, 
    adj = 0.5, 
    outer = TRUE, 
    xpd = TRUE, 
    line = 2
  )
  
  # Add the legend
  legend(
    x = "top", 
    inset = c(0, -0.08), 
    legend = c("Juvenile (%)", "Mature (%)"), 
    lty = c(2, 1), 
    bty = "n", 
    border = NA, 
    cex = 0.7, 
    horiz = TRUE, 
    xpd = TRUE
  )
  
  # Plot the percent Lopt and mega per year
  plot(
    x = as.numeric(froese_dat$year), 
    y = froese_dat$prcntJuv,
    type = "n", 
    axes = FALSE,
    ylim = c(0, 100), 
    xlim = extendedyears,
    xlab = "", 
    ylab = "",
    ...
  )
  
  # Lines and Points
  lines(
    x = froese_dat$year, 
    y = froese_dat$prcntLopt, 
    type = "o", 
    lty = 2,
    ...
  ) 
  
  lines(
    x = froese_dat$year, 
    y = froese_dat$prcntMega, 
    type = "o", 
    lty = 1,
    ...
  ) 
  
  # Axes
  axis(
    side = 1, 
    at = datayears, 
    labels = froese_dat$year, 
    line = 0.8, 
    tcl = -0.3
  )
  
  axis(
    side = 2, 
    at = seq(0, 100, 10), 
    labels = seq(0, 100, 10), 
    las = 1, 
    line = 0.5, 
    tcl = -0.3
  )
  
  # Add plot title
  mtext(
    text = "Lopt and Mega-Spawners", 
    side = 3, 
    line = 0.5, 
    adj = 0, 
    cex = 1.2, 
    outer = TRUE, 
    font = 2
  )
  
  # Add plot subtitle
  mtext(
    text = species, 
    side = 3, 
    line = -0.65, 
    adj = 0, 
    font = 3, 
    cex = 1.1, 
    outer = TRUE
  )
  
  # Add plot caption
  mtext(
    text = "Mega-spawners: fish of a size larger than optimum length plus 10%.", 
    side = 3, 
    line = -1.5, 
    adj = 0, 
    font = 3, 
    cex = 0.65, 
    outer = TRUE
  )
  
  # Add y-axis title
  mtext(
    text = "Percent in Catch", 
    side = 2, 
    adj = 0.5, 
    las = 0, 
    outer = TRUE, 
    line = -0.1
  )
  
  # Add x-axis title
  mtext(
    text = "Years", 
    side = 1, 
    adj = 0.5, 
    outer = TRUE, 
    xpd = TRUE, 
    line = 2
  )
  
  # Adding a legend to distinguish between the lines
  legend(
    x = "top", 
    inset = c(0, -0.05), 
    legend = c("Lopt (%)", "Mega (%)"), 
    lty = c(2, 1), 
    bty = "n", 
    border = NA, 
    cex = 0.7, 
    horiz = TRUE, 
    xpd = TRUE
  )
  
  # Plot the length distribution for all years
  lenVal <- x[, .(length, raised_lf)]
  
  # Round 'raised_lf' to nearest integer
  lenVal[, raised_lf := round(raised_lf)]
  
  # Use data.table to repeat 'length' values based on 'raised_lf' values
  lenVal <- lenVal[, .(length = rep(length, raised_lf))]
  
  # Remove NA values
  lenVal <- na.omit(lenVal)
  
  # Plot the data
  
  # Identify breaks
  # Ensure that the breaks span the range of the data
  brks <- seq(
    from = floor(min(lenVal$length)),  # Start from the minimum of the data
    to = ceiling(max(lenVal$length)),  # End at the maximum of the data
    by = ci  # Step size can be adjusted as needed
  )
  
  # Recalculate breaks if maxL is greater than the maximum of brks
  if (maxL > max(brks)) {  # Ensure maxL is greater than the current maximum of brks
    new_brks <- seq(tail(brks, 1), maxL, by = 1)
    brks <- c(brks, new_brks[-1])  # Exclude the first element to avoid duplication
  }
  
  # Create the histogram with the correct breaks
  histdata <- hist(
    x = lenVal$length,
    breaks = brks,
    plot = FALSE
  )
  
  maxY <- max(histdata$counts)
  maxYLim <- ceiling(maxY + (maxY * 0.2))
  
  # Plot an empty plot with the same x and y limits
  plot(
    x = histdata$counts, 
    type = "n", 
    xlab = "", 
    ylab = "",
    xlim = c(0, maxL), 
    ylim = c(0, maxYLim),
    xaxt = "n", 
    yaxt = "n", 
    bty = "n",
    tcl = -0.3,
    ...
  )
  
  # Add lines to represent the histogram
  lines(
    x = histdata$counts, 
    type = "l",
    ...
  )
  
  # Axes labels
  axis(
    side = 1, 
    at = seq(0, maxL, by = 5), 
    labels = seq(0, maxL, by = 5), 
    las = 1
  )
  
  # Define breaks and labels for the y-axis using scientific notation if needed
  ybreaks <- pretty(histdata$counts)
  ylabels <- ifelse(
    ybreaks > 100, 
    formatC(ybreaks, format = "e", digits = 1), 
    ybreaks
  )
  
  axis(
    side = 2,
    at = ybreaks,
    labels = ylabels,
    las = 1
  )
  
  # Add vertical lines for Lm and Lmax, rectangle for Lopt range
  segments(
    x0 = c(Lm, maxL), 
    y0 = 0, 
    x1 = c(Lm, maxL), 
    y1 = maxY, 
    lwd = 0.9, 
    lty = 3,
    ...
  )
  
  rect(
    xleft = LoptMinus10, 
    ybottom = 0, 
    xright = LoptPlus10, 
    ytop = maxY, 
    lwd = 0.8, 
    border = NA,
    col = rgb(220, 220, 220, alpha = 125, maxColorValue = 255)
  )
  
  # Add text labels in the plot
  text(
    x = Lm, 
    y = maxY * 1.05, 
    labels = "Lm", 
    cex = 0.9
  )
  
  text(
    x = Lopt, 
    y = maxY * 1.05, 
    labels = "Lopt", 
    cex = 0.9
  )
  
  text(
    x = maxL, 
    y = maxY * 1.05, 
    labels = "Lmax", 
    cex = 0.9
  )
  
  text(
    x = mean(c(0, Lm)), 
    y = maxY * 0.15, 
    labels = "Juveniles", 
    cex = 0.9
  )
  
  text(
    x = mean(c(Lm, maxL)), 
    y = maxY * 0.15, 
    labels = "Mature", 
    cex = 0.9
  )
  
  text(
    x = mean(c(LoptPlus10, maxL)), 
    y = maxY * 0.6, 
    labels = "Mega-\nspawners", 
    cex = 0.9
  )
  
  text(
    x = mean(c(LoptMinus10, LoptPlus10)), 
    y = maxY * 0.9, 
    labels = "Optimum\nsize", 
    cex = 0.75, 
    font = 3
  )
  
  # Add title
  mtext(
    text = "Froese Length-Based Sustainability Indicator", 
    side = 3, 
    line = 0.5, 
    adj = 0, 
    cex = 1.2, 
    outer = TRUE, 
    font = 2
  )
  
  # Add plot subtitle
  mtext(
    text = species, 
    side = 3, 
    line = -0.65, 
    adj = 0, 
    font = 3, 
    cex = 1.1, 
    outer = TRUE
  )
  
  # Add plot caption
  mtext(
    text = "Mega-spawners: fish of a size larger than optimum length plus 10%.", 
    side = 3, 
    line = -1.5, 
    adj = 0, 
    font = 3, 
    cex = 0.65, 
    outer = TRUE
  )
  
  # Add y-axis title
  mtext(
    text = "Frequency", 
    side = 2, 
    adj = 0.5, 
    las = 0, 
    outer = TRUE, 
    line = 0.5
  )
  
  # Add x-axis title
  mtext(
    text = "Length (cm)", 
    side = 1, 
    adj = 0.5, 
    outer = TRUE, 
    xpd = TRUE, 
    line = 2
  )
  
  # LBAR PLOTS
  
  # Time series Lc
  tsLc <- ts(data = result$Lc, start = minYear, end = maxYear, frequency = 1)
  
  # Time series Lbar
  tsLbar <- ts(data = result$Lbar, start = minYear, end = maxYear, frequency = 1)
  
  # Plot the time series data for Lc
  plot(
    x = tsLc, 
    type = "o", 
    lty = 2,
    yaxt = "n", 
    ylim = c(0, maxL), 
    xlim = c(min(time(tsLc)), max(time(tsLc)) + 1), 
    xlab = "", 
    ylab = "", 
    bty = "n",
    ...
  )
  
  # Add y-axis labels
  axis(
    side = 2, 
    at = seq(0, maxL, by = 5), 
    labels = seq(0, maxL, by = 5), 
    las = 2, 
    tcl = -0.3
  )
  
  # Add the time series data for Lbar
  lines(
    x = tsLbar, 
    type = "o", 
    lty = 1,
    ...
  )
  
  # Draw horizontal line segment for Lm within the plot area
  segments(
    x0 = minYear, 
    y0 = Lm, 
    x1 = maxYear, 
    y1 = Lm, 
    lty = 3,
    ...
  )
  
  # Add text label for Lm with extended xlim
  text(
    x = maxYear + 0.05, 
    y = Lm, 
    labels = "Lm", 
    pos = 4, 
    cex = 0.8, 
    font = 3
  )
  
  # Add plot title
  mtext(
    text = "Change in Lc and Lbar Over Years", 
    side = 3, 
    line = 0.5, 
    adj = 0, 
    cex = 1.2, 
    outer = TRUE, 
    font = 2
  )
  
  # Add plot subtitle
  mtext(
    text = species, 
    side = 3, 
    line = -0.65, 
    adj = 0, 
    font = 3, 
    cex = 1.1, 
    outer = TRUE
  )
  
  # Add y-axis title
  mtext(
    text = "Length (cm)", 
    side = 2, 
    adj = 0.5, 
    las = 0, 
    outer = TRUE, 
    line = -0.1
  )
  
  # Add x-axis title
  mtext(
    text = "Years", 
    side = 1, 
    adj = 0.5, 
    outer = TRUE, 
    xpd = TRUE, 
    line = 1.5
  )
  
  # Adding a legend to distinguish between the lines
  legend(
    x = "top", 
    inset = c(0, -0.08), 
    legend = c("Lc", "Lbar"), 
    lty = c(2, 1), 
    bty = "n", 
    border = NA, 
    cex = 0.7, 
    horiz = TRUE, 
    xpd = TRUE
  )
  
  # Time series Z
  tsZ <- ts(data = result$Z, start = minYear, end = maxYear, frequency = 1)
  
  # Time series F
  tsF <- ts(data = result$Fm, start = minYear, end = maxYear, frequency = 1)
  
  # Time series F/M
  tsFM <- ts(data = result$`F/M`, start = minYear, end = maxYear, frequency = 1)
  
  # Plot the time series data for Z
  plot(
    x = tsZ, 
    type = "o", 
    lty = 2,
    yaxt = "n", 
    ylim = c(0, ceiling(max(result[, 4:6]) * 1.1)), 
    xlim = c(min(time(tsZ)), max(time(tsZ)) + 1), 
    xlab = "", 
    ylab = "", 
    bty = "n",
    ...
  )
  
  # Add y-axis labels
  axis(
    side = 2, 
    at = seq(0, ceiling(max(result[, 4:6]) * 1.1), by = 5), 
    labels = seq(0, ceiling(max(result[, 4:6]) * 1.1), by = 5), 
    las = 2, 
    tcl = -0.3
  )
  
  # Add the time series data for F
  lines(
    x = tsF, 
    type = "o", 
    lty = 3,
    ...
  )
  
  # Add the time series data for F/M
  lines(
    x = tsFM, 
    type = "o", 
    lty = 1,
    ...
  )
  
  # Add plot title
  mtext(
    text = "Change in Z, F, and F/M Over Years", 
    side = 3, 
    line = 0.5, 
    adj = 0, 
    cex = 1.2, 
    outer = TRUE, 
    font = 2
  )
  
  # Add plot subtitle
  mtext(
    text = species, 
    side = 3, 
    line = -0.65, 
    adj = 0, 
    font = 3, 
    cex = 1.1, 
    outer = TRUE
  )
  
  # Add y-axis title
  mtext(
    text = "Index", 
    side = 2, 
    adj = 0.5, 
    las = 0, 
    outer = TRUE, 
    line = -0.1
  )
  
  # Add x-axis title
  mtext(
    text = "Years", 
    side = 1, 
    adj = 0.5, 
    outer = TRUE, 
    xpd = TRUE, 
    line = 1.5
  )
  
  # Adding a legend to distinguish between the lines
  legend(
    x = "top", 
    inset = c(0, -0.08), 
    legend = c("Z", "F", "F/M"), 
    lty = c(2, 3, 1), 
    bty = "n", 
    border = NA, 
    cex = 0.7, 
    horiz = TRUE, 
    xpd = TRUE
  )
  
  cat("--------------------------------------------------\n")
  cat("NOTE: \n")
  message("The plots are displayed in the 'Plots' panel.")
  message("You can navigate through the plots using the arrow keys next to the Zoom option in the Plots panel.")
  message("Lastly, you can save each plot by clicking the 'Export' button.")
  
  message("The files are exported as CSV in the 'output' directory.")
}