#' @title Estimating Initial Value of Asymptotic Length
#' 
#' @description
#' Function to calculate the initial estimate of the asymptotic length of the
#' species, according to two empirical formulas.
#' 
#' @param Lmax The maximum length of the species in the data.
#' 
#' @details
#' The empirical formulas were adapted from the works of Pauly (1983, 1984) and
#' Froese and Binohlan (2000).
#' 
#' @return A value of estimated asymptotic length.
#' 
#' @references 
#' Froese R, Binohlan C. 2000. Empirical relationships to estimate asymptotic 
#' length, length at first maturity and length at maximum yield per recruit in 
#' fishes, with a simple method to evaluate length frequency data. Journal of 
#' Fish Biology. 56(4):758–773. doi:10.1111/j.1095-8649.2000.tb00870.x.
#' 
#' Pauly D. 1983. Some simple methods for the assessment of tropical fish stocks. 
#' Rome: Food and Agriculture Organization of the United Nations (FAO fisheries 
#' technical paper). https://www.fao.org/3/X6845E/X6845E00.htm.
#' 
#' Pauly D. 1984. Fish population dynamics in tropical waters: a manual for use 
#' with programmable calculators. Manila, Philippines: ICLARM 
#' (ICLARM Studies and Reviews).
#' 
#' @import data.table
#' 
#' @export
#' @examples
#' \dontrun{
#'
#' library(fsaR)
#' library(data.table)
#' 
#' Lmax <- 24.5
#'
#' initLinf(Lmax)
#' 
#' initLinf(20.56)
#'
#' }
#'

initLinf <- function(Lmax = NULL){
  
  Linf <- data.frame(
    pauly1983 = round(Lmax / 0.95, digits = 2),
    pauly1984 = round(1.05 * Lmax, digits = 2),
    froese2000 = round(10^(0.044 + 0.9841 * log10(Lmax)), digits = 2)
  )
  
  return(Linf)
}